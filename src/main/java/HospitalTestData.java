import Mappe.del1.hospital.*;
import Mappe.del1.hospital.healthpersonal.*;
import Mappe.del1.hospital.healthpersonal.doctor.GeneralPractitioner;
import Mappe.del1.hospital.healthpersonal.doctor.Surgeon;


public class HospitalTestData {
   private HospitalTestData() {
        // not called
    }

    /**
     * Fills a hospital object with data.
     * @param hospital The hospital to fill the test data with
     */
    public static void fillRegisterWithTestData(final Hospital hospital) {

        /*
        Since I choose to implement HashMaps to hold the employees and patients in a department,
        som of the test data had to be altered. Does the same thing as the one handed out, but uses
        the add-methods from the ER-diagrams. Have also added a personal number to each person as a requirements
        set by the HashMaps.
         */

        // Add some departments
        Department emergency = new Department("Akutten");
        emergency.addEmployee(new Employee("Odd Even", "Primtallet", "0"));
        emergency.addEmployee(new Employee("Huppasahn", "DelFinito", "1"));
        emergency.addEmployee(new Employee("Rigmor", "Mortis", "2"));
        emergency.addEmployee(new GeneralPractitioner("Inco", "Gnito", "3"));
        emergency.addEmployee(new Surgeon("Inco", "Gnito", "4"));
        emergency.addEmployee(new Nurse("Nina", "Teknologi", "5"));
        emergency.addEmployee(new Nurse("Ove", "Ralt", "6"));
        emergency.addPatient(new Patient("Inga", "Lykke", "7"));
        emergency.addPatient(new Patient("Ulrik", "Smål", "8"));

        emergency.getPatients().next().setDiagnosis("Flu");

        hospital.getDepartments().add(emergency);

        Department childrenPolyclinic = new Department("Barn poliklinikk");
        childrenPolyclinic.addEmployee(new Employee("Salti", "Kaffen", "0"));
        childrenPolyclinic.addEmployee(new Employee("Nidel V.", "Elvefølger", "1"));
        childrenPolyclinic.addEmployee(new Employee("Anton", "Nym", "2"));
        childrenPolyclinic.addEmployee(new GeneralPractitioner("Gene", "Sis", "3"));
        childrenPolyclinic.addEmployee(new Surgeon("Nanna", "Na", "4"));
        childrenPolyclinic.addEmployee(new Nurse("Nora", "Toriet", "5"));
        childrenPolyclinic.addPatient(new Patient("Hans", "Omvar", "6"));
        childrenPolyclinic.addPatient(new Patient("Laila", "La", "7"));
        childrenPolyclinic.addPatient(new Patient("Jøran", "Drebli", "8"));

        childrenPolyclinic.getPatients().next().setDiagnosis("Corona");

        hospital.getDepartments().add(childrenPolyclinic);
    }
}
