package Mappe.del1.hospital;
import Mappe.del1.hospital.exception.RemoveException;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Represents a department in a hospital.
 *
 */
public class Department {
    private String departmentName;
    private final HashMap<String,Employee> employees;
    private final HashMap<String,Patient> patients;

    /**
     * Creates a department with a specified name. Also initializes two HashMaps with
     * all the employees and patients connected to the hospital
     * @param departmentName The name of the department
     */
    public Department(String departmentName){
        this.departmentName = departmentName;
        this.employees = new HashMap<>();
        this.patients = new HashMap<>();
    }

    /**
     * Adds an employee to the list of employees connected to the department.
     * Checks if the employee already exists in the list based on the social-
     * security number, before adding.
     * @param employee An employee object to be added to the list
     */
    public void addEmployee(Employee employee){
        if (!this.employees.containsKey(employee.getSocialSecurityNumber()))
            this.employees.put(employee.getSocialSecurityNumber(), employee);
    }

    /**
     * Adds a patient to the list of patients connected to the department
     * Checks if the patient already exists in the list based on the social-
     * security number, before adding.
     * @param patient A patient object to be added to the list
     */
    public void addPatient(Patient patient){
        if (!this.patients.containsKey(patient.getSocialSecurityNumber()))
            this.patients.put(patient.getSocialSecurityNumber(), patient);
    }

    /**
     * Gets the name of the department
     * @return A string with the name of the department
     */
    public String getDepartmentName(){ return departmentName; }

    /**
     * Gets all the employees connected to the department
     * @return An iterator with all the employees connected to the department
     */
    public Iterator<Employee> getEmployees(){
        return employees.values().iterator();
    }
    /**
     * Gets all the patients connected to the department
     * @return An iterator with all the patients connected to the department
     */
    public Iterator<Patient> getPatients(){
        return this.patients.values().iterator();
    }

    /**
     * Sets the name of the department
     * @deprecated Method made according to the ER-diagram, but never used
     * @param newName A String with the name of the department
     */
    public void setDepartmentName(String newName){
        this.departmentName = newName;
    }

    /**
     * Removes an instance of a Person from the employee or patients list
     * @param person A Person object to be removed from the employee/patients list
     * @throws RemoveException if the Person object do not exist in any of the lists connected to the department
     */
    public void remove (Person person) throws RemoveException {
        if (person instanceof Employee && this.employees.containsKey(person.getSocialSecurityNumber())) {
            this.employees.remove(person.getSocialSecurityNumber());
        } else if (person instanceof Patient && this.patients.containsKey(person.getSocialSecurityNumber())) {
            this.patients.remove(person.getSocialSecurityNumber());
        } else {
            throw new RemoveException( person.getFullName() + " does not exist in this Hospital" );
        }
    }

    /**
     * Overrides the standard equals method.
     * Checks if the passed object is a department and if it is the same as the current department.
     * Made as a part of the methods presented in the ER-diagram.
     * @param obj An Object to be compared
     * @return The department and its connected employees and patients if it passes tge check
     */
    @Override
    public boolean equals(Object obj){
        if (this == obj) return true;
        if (obj == null) return false;
        if ((obj instanceof Person)) return false;
        Department department = (Department) obj;
        return departmentName.equals(department.departmentName) &&
                patients.equals(department.patients) &&
                employees.equals(department.employees);

    }

    /**
     * Overrides the standard hash method.
     * @return A hash to be used when a Person is added to a list
     */
    @Override
    public int hashCode(){
        int hash = 31;
        hash = 31 * hash + 17;
        hash = 31 * hash + departmentName.hashCode();
        return hash ;
    }

    /**
     * ToString method. Overrides the standard toString method.
     * @return All the information given in the constructor + the employees and patients connected to teh department
     */
    @Override
    public String toString() {
        return "Department{" +
                "departmentName='" + departmentName + '\'' +
                ", employees=" + employees +
                ", patients=" + patients +
                '}';
    }
}
