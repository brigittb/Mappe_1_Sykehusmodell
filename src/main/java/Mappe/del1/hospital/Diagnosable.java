package Mappe.del1.hospital;

/**
 * Represents Diagnosable; an interface that set diagnosis
 * Only contains one method that need to be implemented by
 * classes that implements the interface
 */
public interface Diagnosable {
    default void setDiagnosis(String diagnosis) {

    }
}
