package Mappe.del1.hospital;

/**
 * Represents a Patient, an extension/subclass of the Person class.
 * Implements the class Diagnosable
 */

public class Patient extends Person implements Diagnosable {
    private String diagnosis = "";

    /**
     *  A subclass of Person. Creates a patient with specified name and social security number
     * @param firstname The patients firstname
     * @param lastname The patients lastname
     * @param socialSecurityNumber The patients social security number
     */
    public Patient(String firstname, String lastname, String socialSecurityNumber) {
        super(firstname, lastname, socialSecurityNumber);
    }

    /**
     * Gets the patients diagnosis
     * @return A string with the patient's set diagnosis
     */
    public String getDiagnosis() {
        if(diagnosis.equals("")) return "no illness";
        else return diagnosis;
    }

    /**
     * Sets the patients diagnosis. Overrides the same method in the class <Diagnosable>Diagnosable</Diagnosable>
     * and can only be set by a Doctor
     * @param diag A String containing the patient's diagnosis
     */
    @Override
    public void setDiagnosis(String diag) {
       this.diagnosis = diag;
    }

    /**
     * ToString method. Overrides the toString method from Person
     * @return All the information given in the constructor
     */
    @Override
    public String toString() {
        return "Patient: " + super.toString() + ", " + "is diagnosed with: " + getDiagnosis();
    }
}
