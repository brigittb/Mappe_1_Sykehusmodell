package Mappe.del1.hospital;

/**
 * Represents a person
 * Overwritten by subclasses
 */

 public abstract class Person {
    private String firstname;
    private String lastname;
    private String socialSecurityNumber;

    /**
     * Creates an abstract Person. Can't be initialized, but defines the required
     * parameters for other classes that extends from this.
     * @param firstname The person's first name.
     * @param lastname The person's last name
     * @param socialSecurityNumber The person's identifying security number
     */
    public Person(String firstname, String lastname, String socialSecurityNumber){
        this.firstname = firstname;
        this.lastname = lastname;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * Gets the persons firstname
     * @return A string representing the person's first name.
     */
    public String getFirstname() { return firstname; }

    /**
     * Gets the persons lastname
     * @return A string representing the person's last name.
     */
    public String getLastname() { return lastname; }

    /**
     * Gets the persons full name
     * @return A string representing the person's full name, both first- and lastname.
     */
    public String getFullName() { return firstname + " " + lastname; }

    /**
     * Gets the persons security number
     * @return A string representing the person's security number.
     */
    public String getSocialSecurityNumber() { return socialSecurityNumber; }

    /**
     * Sets the person's firstname
     * @deprecated Method made according to the ER-diagram, but never used
     * @param first A String containing the persons firstname
     */
    void setFirstname(String first){ this.firstname = first; }

    /**
     * Sets the person's lastname
     * @deprecated Method made according to the ER-diagram, but never used
     * @param last A String containing the persons lastname
     */
    void setLastname(String last){ this.lastname = last; }

    /**
     * Sets the person's security number
     * @deprecated Made according to the ER-diagram, but never used
     * @param socialSecNumber A String containing the persons security number
     */
    void setSocialSecNumber(String socialSecNumber){ this.socialSecurityNumber = socialSecNumber; }

    /**
     * ToString method
     * @return All the information given in the constructor
     */
    public String toString(){
        return "Firstname: " + getFirstname() +
                "\" Lastname: " + getLastname() +
                "\" Social security number: " + getSocialSecurityNumber();
    }
}
