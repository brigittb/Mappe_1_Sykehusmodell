package Mappe.del1.hospital.healthpersonal.doctor;

import Mappe.del1.hospital.Diagnosable;
import Mappe.del1.hospital.Employee;
import Mappe.del1.hospital.Patient;

/**
 * Represents a Doctor, an extension/subclass of the Employee  class.
 * Implements the class Diagnosable
 */
public abstract class Doctor extends Employee implements Diagnosable {

    /**
     * A subclass of Employee. Creates an abstract Doctor with specified name and social security number
     * Can't be initialized, but defines the required parameters for other classes that extends from this.
     *
     * @param firstname            The employee's firstname
     * @param lastname             The employee's lastname
     * @param socialSecurityNumber The employee's social security number
     */
    public Doctor(String firstname, String lastname, String socialSecurityNumber) {
        super(firstname, lastname, socialSecurityNumber);
    }

    /**
     * Sets a patients diagnosis
     * @param patient The patient to have their diagnosis set
     * @param diagnosis A String wth the diagnosis
     */
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}
