package Mappe.del1.hospital.healthpersonal.doctor;
import Mappe.del1.hospital.Patient;

/**
 * Represents a General Practitioner, an extension/subclass of the Doctor class.
 * Implements the class Diagnosable
 */
public class GeneralPractitioner extends Doctor{

    /**
     * A subclass of Doctor. Creates a general practitioner with specified name and social security number
     * @param firstname The practitioner's firstname
     * @param lastname The practitioner's lastname
     * @param socialSecurityNumber The practitioner's social security number
     */
    public GeneralPractitioner(String firstname, String lastname, String socialSecurityNumber) {
        super(firstname, lastname, socialSecurityNumber);
    }

    /**
     * Sets a patients diagnosis
     * @param patient The patient to have their diagnosis set
     * @param diagnosis A String wth the diagnosis
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
        super.setDiagnosis(patient, diagnosis);
    }

    /**
     * ToString method. Overrides the toString method from Employee
     * @return All the information given in the constructor
     */
    @Override
    public String toString() {
        return "General practitioner: " + super.toString();
    }
}
