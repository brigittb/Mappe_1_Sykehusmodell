package Mappe.del1.hospital.healthpersonal;
import Mappe.del1.hospital.Employee;

/**
 * Represents a Nurse. A subclass of Employee --> Person
 */
public class Nurse extends Employee {

    /**
     *  A subclass of Employee. Creates a nurse with specified name and social security number
     * @param firstname The nurse's firstname
     * @param lastname The nurse's lastname
     * @param socialSecurityNumber The nurse's social security number
     */
    public Nurse(String firstname, String lastname, String socialSecurityNumber) {
        super(firstname, lastname, socialSecurityNumber);
    }

    /**
     * ToString method. Overrides the toString method from Employee
     * @return All the information given in the constructor
     */
    @Override
    public String toString() {
        return " Nurse: " +super.toString() ;
    }
}
