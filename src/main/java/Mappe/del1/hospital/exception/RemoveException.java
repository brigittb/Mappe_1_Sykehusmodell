package Mappe.del1.hospital.exception;

import java.io.Serial;

/**
 * Represents an exception handler
 */

public class RemoveException extends Exception {
    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * A subclass of Exception. Handles exception when trying to remove a person object
     * that doesn't exist.
     * @param message A String with the exception message
     */
    public RemoveException(String message){
        super(message);
    }
}
