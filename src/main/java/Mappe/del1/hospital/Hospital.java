package Mappe.del1.hospital;
import java.util.ArrayList;

/**
 * Represents a Hospital containing several departments.
 *
 */

public class Hospital {
    private final String hospitalName;
    private final ArrayList<Department> departments = new ArrayList<>();

    /**
     * Creates a hospital with a specified name
     * @param hospitalName The name of the hospital
     */
    public Hospital(String hospitalName){
        this.hospitalName = hospitalName;
    }

    /**
     * Gets the name of the hospital
     * @return A string with the name of the hospital
     */
    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * Gets all the departments connected to the hospital
     * @return An arraylist with all the departments connected to the hospital
     */
    public ArrayList<Department> getDepartments() {
        return departments;
    }

    /**
     * ToString method. Overrides the toString method from Person
     * @return All the information given in the constructor
     */
    @Override
    public String toString() {
        return "Hospital{" +
                "hospitalName='" + hospitalName + '\'' +
                ", departments=" + departments +
                '}';
    }
}
