package Mappe.del1.hospital;

/**
 * Represents an Employee, an extension/subclass of the Person class.
 *
 */
public class Employee extends Person {

    /**
     *  A subclass of Person. Creates an employee with specified name and social security number
     * @param firstname The employee's firstname
     * @param lastname The employee's lastname
     * @param socialSecurityNumber The employee's social security number
     */
    public Employee(String firstname, String lastname, String socialSecurityNumber) {
        super(firstname, lastname, socialSecurityNumber);
    }
    /**
     * ToString method. Overrides the toString method from Person
     * @return All the information given in the constructor
     */
    @Override
    public String toString() {
        return  super.toString() + "\"";
    }
}
