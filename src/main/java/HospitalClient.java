import Mappe.del1.hospital.*;
import Mappe.del1.hospital.exception.RemoveException;

import java.util.Iterator;

/**
 * A Hospital client
 * 1. Prints out every item in the hospital
 * 2. Removes an employee from a Department
 * 3. Tries to remove a patient that does not exist in the hospital. Throws an exception
 */

public class HospitalClient {
    private static void print(Hospital hospital){
        int iter = 1;

        // Prints out every item of the hospital in a structured way
        System.out.println("Navn på sykehus: " + hospital.getHospitalName() + "\n");
        for (Department department : hospital.getDepartments()) {
            System.out.println("Avdeling " + (hospital.getDepartments().indexOf(department) + 1) + ": " + department.getDepartmentName());
            System.out.println("Employees ");
            for (Iterator<Employee> employee = department.getEmployees(); employee.hasNext(); ) {
                System.out.println("Employee nr " + iter + ": " + employee.next());
                iter++;

                if (!employee.hasNext()) System.out.println("Total of employees: " + (iter - 1));

            }
            iter = 1;

            System.out.println("\nPatients");
            for (Iterator<Patient> patient = department.getPatients(); patient.hasNext(); ) {
                System.out.println("Patient nr " + iter + ": " + patient.next());
                iter++;
                if (!patient.hasNext()) System.out.println("Total of patients: " + (iter - 1));
            }
            iter = 1;
            System.out.println();
        }
    }
    public static void main(String[] args) {
        Hospital hospital = new Hospital("St.Olavs Hospital");
        HospitalTestData.fillRegisterWithTestData(hospital);

        // 1. Prints every item in the hospital
        print(hospital);

        // 2. Removes an employee from a department
        try {
            Employee test = new Employee("Odd Even", "Primtallet", "0");
            for (Department department : hospital.getDepartments()) {
                department.remove(test);
            }
            System.out.println( test.getFullName() + " is successfully removed from " + hospital.getHospitalName());

        }catch (RemoveException r){
            System.out.println(r);
        }

        // 3. Tries to remove non-existing patient
        try {
            for (Department department : hospital.getDepartments()) {
                department.remove(new Patient("Brigitt", "Bright", "100"));
            }
        }catch (RemoveException r){
            System.out.println(r);
        }
    }
}
