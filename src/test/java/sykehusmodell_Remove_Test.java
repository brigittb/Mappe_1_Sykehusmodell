import Mappe.del1.hospital.Department;
import Mappe.del1.hospital.Employee;
import Mappe.del1.hospital.Patient;
import Mappe.del1.hospital.exception.RemoveException;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import static org.junit.jupiter.api.Assertions.*;


public class sykehusmodell_Remove_Test {
    Employee employee = new Employee("Brig", "Breit", "000222");
    Patient patient = new Patient("Eddie", "Lake", "555444");
    Department department = new Department("Hjerteavdeling");

    @Test
    public void objectIsPartOfDepartment_nothing_should_be_thrown() {
        department.addEmployee(this.employee);
        department.addPatient(this.patient);
        assertDoesNotThrow(() -> department.remove(employee));
        assertDoesNotThrow(() -> department.remove(patient));
    }

    @Test
    public void objectIsNotPartOfDepartment_should_throw_RemoveException(){
        department.addEmployee(this.employee);
        department.addPatient(this.patient);
        assertThrows(RemoveException.class, () -> department.remove(new Employee("Finnes", "Ikke", "000000")));
        assertThrows(RemoveException.class, () -> department.remove(new Patient("Finnes", "Ikke", "000000")));
    }
}
